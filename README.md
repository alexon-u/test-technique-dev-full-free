## Objectif
Développer une **Application de Gestion de tickets**.

<img src="demo.png" />

le stack technique : **Python/Django et React**

### Outils utilisés
- vite : pour la crétaion de frontend avec React & TypeScript
- redux : pour le state management
- react-hook-form : pour valider les formulaires dans React
- react-router-dom : Routing
- axios : library HTTP client
- postman : HTTP client pour tester les endpoints
- docker : packager l'application avec les dépendances dans une image Docker

#### Informations pour lancer les Applications en locale

- Pour lancer le REST API Django Veuillez référer à ce [back/README.md](./back/README.md)
- Pour lancer l'Application Web pour la Gestion de Tickets, Veuillez référer à ce [front/README.md](./front/README.md)

- ##### **Recommandé d'utiliser Docker-compose pour allumer les Apps**
  - faites un ```git checkout production```, puis lancer la commande ```docker-compose up -d --build```
  - Vous pouvez allez sur [http://localhost:8000/admin](http://localhost:8000/admin) et vous identifier avec le username : "admin" et mot de passe : "admin123"
  - Vous pouvez allez sur le frontend [http://localhost:5173](http://localhost:5173) et vous identifier avec le username : "admin" et mot de passe : "admin123" ou vous créer un user grâce à l'Admin Django