from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics, views, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .models import Ticket
from .serializers import TicketSerializer

# Create your views here.


def index(request):
    return HttpResponse("Gestion des tickets")


class TicketAPIView(views.APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            # Get Only User's tickets
            return Ticket.objects.get(id=pk, user=self.request.user)
        except Ticket.DoesNotExist:
            return None

    def get(self, request, pk):
        ticket = self.get_object(pk)
        if ticket is None:
            return Response({"error": "Ticket not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = TicketSerializer(ticket)
        return Response(serializer.data)

    def put(self, request, pk):
        ticket = self.get_object(pk)
        if ticket is None:
            return Response({"error": "Ticket not found"}, status=status.HTTP_404_NOT_FOUND)

        serializer = TicketSerializer(ticket, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        ticket = self.get_object(pk)
        if ticket is None:
            return Response({"error": "Ticket not found"}, status=status.HTTP_404_NOT_FOUND)

        ticket.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TicketListAPIView(generics.ListCreateAPIView):
    serializer_class = TicketSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        # Get Only User's tickets
        user = self.request.user
        queryset = Ticket.objects.filter(user=user)

        # Filter by Zone
        zone = self.request.query_params.get('zone')
        if zone:
            queryset = queryset.filter(zone=zone)

        # Sort by gived Query Param
        sort_param = self.request.GET.get("sort")
        if sort_param == 'priority':
            queryset = queryset.order_by('priority')
        elif sort_param == 'zone':
            queryset = queryset.order_by('zone')
        # Add more sorting options as needed
        return queryset
