from django.urls import path

from . import views

app_name = "tickets"

urlpatterns = [
    path("index", views.index, name="index"),
    path('', views.TicketListAPIView.as_view(), name='ticket-list'),
    path('<int:pk>', views.TicketAPIView.as_view(),
         name='ticket-retrieve-update-delete'),
]
