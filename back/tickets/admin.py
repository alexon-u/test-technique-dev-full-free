from django.contrib import admin

from .models import Ticket


class TicketAdmin(admin.ModelAdmin):
    list_display = ('id', 'priority', 'zone', 'user')
    list_filter = ('priority', 'zone', 'user')
    search_fields = ('id', 'priority', 'zone', 'user__name')
    ordering = ('id',)


# Register your models here.
admin.site.register(Ticket, TicketAdmin)
