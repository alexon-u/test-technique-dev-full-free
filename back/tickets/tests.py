from django.test import TestCase
from django.contrib.auth.models import User
from tickets.models import Ticket

# Create your tests here.

url = "http://127.0.0.1:8000"


class ViewsTestCase(TestCase):
    def setUp(self):
        """Cette méthode se lance avant les tests unitaires"""
        username = "test"
        email = "test@gmail.com"
        password = "G8!g@0PcQKP#"
        user = User.objects.create_user(
            username=username, email=email, password=password)
        self.id_user = user.id
        self.user = user
        self.user.password = password
        self.access_token = None
        self.refresh_token = None

        self.authenticate_user(self.user)

    def authenticate_user(self, user):
        """Se connecter"""
        payload = {"username": user.username,
                   "password": user.password}
        response = self.client.post(
            f'{url}/auth/api/token', data=payload)
        # 200 tout va bien
        self.assertEqual(response.status_code, 200)

        # set tokens
        self.access_token = response.data["access"]
        self.refresh_token = response.data["refresh"]

    def test_fail_login_user(self):
        """Connexion Non autorisé"""
        payload = {"username": "john", "password": "john123"}
        response = self.client.post(
            f'{url}/auth/api/token', data=payload)

        # 401 Unauthorized
        self.assertEqual(response.status_code, 401)

    def test_add_ticket(self):
        """Ajouter un ticket avec succès"""
        payload = {
            "priority": 2,
            "zone": "D"
        }
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        response = self.client.post(
            f'{url}/tickets/', data=payload, headers=headers)

        # ajouter un ticket avec succès
        self.assertEqual(response.status_code, 201)

    def test_add_with_invalid_ticket(self):
        """Ajouter un ticket avec un payload data invalide"""
        payload = {
            "priority": 2,
            "zone": "paris-13"
        }
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        response = self.client.post(
            f'{url}/tickets/', data=payload, headers=headers)

        # 400 Bad Request
        self.assertEqual(response.status_code, 400)

    def test_list_ticket(self):
        """lister les tickets"""
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        response = self.client.get(
            f'{url}/tickets/', headers=headers)

        self.assertEqual(response.status_code, 200)

    def test_retrieve_ticket(self):
        """obtenir un ticket"""
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        ticket = Ticket.objects.create(priority=1, zone="A", user=self.user)
        response = self.client.get(
            f'{url}/tickets/{ticket.id}', headers=headers)

        self.assertEqual(response.status_code, 200)

    def test_retrieve_non_persisted_ticket(self):
        """obtenir un ticket qui n'existent pas dans la bdd"""
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        response = self.client.get(
            f'{url}/tickets/100', headers=headers)

        # 404 Not Found
        self.assertEqual(response.status_code, 404)

    def test_update_ticket(self):
        """màj un ticket"""
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        ticket = Ticket.objects.create(priority=1, zone="A", user=self.user)
        response = self.client.put(
            f'{url}/tickets/{ticket.id}',
            data={"priority": 3, "zone": "A"},
            content_type="application/json",
            headers=headers
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['priority'], 3)

    def test_delete_ticket(self):
        """supprimer un ticket"""
        headers = {
            "Authorization": f"Bearer {self.access_token}"
        }
        ticket = Ticket.objects.create(priority=1, zone="A", user=self.user)
        response = self.client.delete(
            f'{url}/tickets/{ticket.id}',
            headers=headers
        )

        self.assertEqual(response.status_code, 204)
