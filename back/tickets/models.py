from django.db import models
from django.contrib.auth.models import User
from string import ascii_uppercase


# Create your models here.

class Ticket(models.Model):
    PRIORITY_CHOICES = [
        (1, "High priority"),
        (2, "Medium priority"),
        (3, "Low priority"),
    ]
    ZONE_CHOICES = [(i, i) for i in ascii_uppercase]

    priority = models.IntegerField(choices=PRIORITY_CHOICES)
    zone = models.CharField(max_length=1, choices=ZONE_CHOICES)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.id)
