import pytest
from django.contrib.auth.models import User
from django.test import Client
from tickets.models import Ticket

url = "http://127.0.0.1:8000"


@pytest.fixture
def client():
    return Client()


@pytest.fixture
def user(db):
    username = "test"
    email = "test@gmail.com"
    password = "G8!g@0PcQKP#"
    user = User.objects.create_user(
        username=username, email=email, password=password)
    user.password = password
    return user


@pytest.fixture
def tokens(client, user):
    payload = {"username": user.username, "password": user.password}
    response = client.post(f'{url}/auth/api/token', data=payload)
    assert response.status_code == 200
    return response.json()


@pytest.mark.django_db
def test_ticket_model():
    ticket = Ticket.objects.create(priority=1, zone="A")
    assert ticket.priority == 1
    assert ticket.zone == "A"


def test_fail_login_user(client, db):
    payload = {"username": "john", "password": "john123"}
    response = client.post(f'{url}/auth/api/token', data=payload)
    assert response.status_code == 401


def test_add_ticket(client, tokens, db):
    payload = {"priority": 2, "zone": "D"}
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    response = client.post(f'{url}/tickets/', data=payload, headers=headers)
    assert response.status_code == 201


def test_add_with_invalid_ticket(client, tokens, db):
    payload = {"priority": 2, "zone": "paris-13"}
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    response = client.post(f'{url}/tickets/', data=payload, headers=headers)
    assert response.status_code == 400


def test_list_ticket(client, tokens, db):
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    response = client.get(f'{url}/tickets/', headers=headers)
    assert response.status_code == 200


def test_retrieve_ticket(client, tokens, db, user):
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    ticket = Ticket.objects.create(priority=1, zone="A", user=user)
    response = client.get(f'{url}/tickets/{ticket.id}', headers=headers)
    assert response.status_code == 200


def test_retrieve_non_persisted_ticket(client, tokens, db):
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    response = client.get(f'{url}/tickets/100', headers=headers)
    assert response.status_code == 404


def test_update_ticket(client, tokens, db, user):
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    ticket = Ticket.objects.create(priority=1, zone="A", user=user)
    response = client.put(
        f'{url}/tickets/{ticket.id}',
        data={"priority": 3, "zone": "A"},
        content_type="application/json",
        headers=headers
    )
    assert response.status_code == 200
    assert response.json()['priority'] == 3


def test_delete_ticket(client, tokens, db, user):
    headers = {"Authorization": f"Bearer {tokens['access']}"}
    ticket = Ticket.objects.create(priority=1, zone="A", user=user)
    response = client.delete(f'{url}/tickets/{ticket.id}', headers=headers)
    assert response.status_code == 204
