#### Pour Lancer le backend avec un environnement virtuel local
```
$ cd back
$ python -m venv env
$ source env/scripts/activate
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser (admin)
$ python manage.py runserver
```

#### Lancer les tests
```
$ python manage.py test (lancer le test avec la bibliothèque de tests de Django)
$ pytest (lancer le test avec la bibliothèque Pytest)
```

#### Utiliser Postman pour tester les requêtes
- une fois que vous avez crée votre user dans la console Admin en allant sur la page **/admin**
- Connectez vous avec les identifiants que vous avez crées juste avant avec la commande **createsuperuser**
- Créer un user
- Importez la collection dans postman [docs/test-technique-dev-full-free.postman_collection.json](docs/test-technique-dev-full-free.postman_collection.json)
- Allez-y maintenant sur La collection postman, et précissément sur la requête **auth-login -> Body -> raw -> remplacer les identifiants par les vôtres**.
- prenez le access token et utiliser la dans les requêtes sur la Gestion de tickets.
- Exemple : **create-ticket request -> authorization -> Bearer Token -> mettez le access token**.

#### Avec la documentation Swagger (sous format OpenApi 3 specifications), nous pouvons générer du code Client-Side (Javascript/Typescript)
- créer la documentation avec le package **drf-spectacular**
- télecharger le jar swagger-codegen-cli ```wget https://repo1.maven.org/maven2/io/swagger/codegen/v3/swagger-codegen-cli/3.0.45/swagger-codegen-cli-3.0.45.jar -O swagger-codegen-cli.jar```
- télécharger le schema (openapi 3) : 
  - ```python manage.py spectacular --file schema.json ( ou schema.yaml)```
  - ```curl 'http://127.0.0.1:8000/api/schema?format=json' > schema.json```
- génerer l'api client 
  - ```java -jar swagger-codegen-cli.jar generate -i schema.json -l javascript -o client-api```
  - ```java -jar swagger-codegen-cli.jar generate -i schema.json -l typescript-axios -o client-api```

ou avec **openapi-generator** (recommandé)
- ```wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/6.6.0/openapi-generator-cli-6.6.0.jar -O openapi-generator-cli.jar```
- ```java -jar openapi-generator-cli.jar generate -i schema.yaml -g typescript-axios -o open-api --additional-properties=npmName=testing,npmVersion=1.0.0```