interface TicketState {
  id?: number,
  priority: number,
  zone: string
}

export default TicketState;