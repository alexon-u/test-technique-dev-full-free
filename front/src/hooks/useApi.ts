import { useNavigate } from "react-router-dom";
import axios from "axios";

const useApi = () => {
  const navigate = useNavigate();

  const api = axios.create({
    baseURL: import.meta.env.API_URL,
  });

  api.interceptors.request.use(async (config) => {
    const accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      config.headers["Authorization"] = `Bearer ${accessToken}`;
    }
    return config;
  });

  api.interceptors.response.use(
    (response) => response,
    async (error) => {
      const originalRequest = error.config;
      const refreshToken = localStorage.getItem("refreshToken");

      if (error.response.status === 401 && refreshToken) {
        try {
          const response = await api.post("/auth/api/token/refresh", {
            refresh: refreshToken,
          });

          localStorage.setItem("accessToken", response.data.access);

          originalRequest.headers[
            "Authorization"
          ] = `Bearer ${response.data.access}`;

          return api(originalRequest);
        } catch (error) {
          localStorage.removeItem("accessToken");
          localStorage.removeItem("refreshToken");
          navigate("/login");
        }
      }

      return Promise.reject(error);
    }
  );

  return api;
};

export default useApi;
