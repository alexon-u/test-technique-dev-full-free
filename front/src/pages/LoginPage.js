import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import { useNavigate } from "react-router-dom";
import useApi from "../hooks/useApi";
import { useForm } from "react-hook-form";
const LoginPage = () => {
    const { register, handleSubmit, formState: { errors }, } = useForm();
    const navigate = useNavigate();
    const api = useApi();
    const onSubmit = async (data) => {
        try {
            const response = await api.post("/auth/api/token", data);
            localStorage.setItem("accessToken", response.data.access);
            localStorage.setItem("refreshToken", response.data.refresh);
            navigate("/tickets");
        }
        catch (error) {
            console.error(error);
        }
    };
    return (_jsxs("form", { onSubmit: handleSubmit(onSubmit), children: [_jsxs("label", { children: ["Username:", _jsx("input", { type: 'text', ...register("username", { required: "Username is required" }) }), errors.username && _jsx("p", { children: errors.username.message })] }), _jsxs("label", { children: ["Password:", _jsx("input", { type: 'password', ...register("password", { required: "Password is required" }) }), errors.password && _jsx("p", { children: errors.password.message })] }), _jsx("button", { type: 'submit', children: "Login" })] }));
};
export default LoginPage;
