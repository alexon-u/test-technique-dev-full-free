import { jsx as _jsx } from "react/jsx-runtime";
const HomePage = () => {
    return _jsx("div", { children: "HomePage" });
};
export default HomePage;
