import TicketList from "../components/TicketList";
import TicketForm from "../components/TicketForm";
import useApi from "../hooks/useApi";
import { useAppDispatch } from "../store/hooks";
import {
  AddTicket,
  DeleteTicket,
  UpdateTicket,
} from "../store/slices/ticketsSlice";
import { useNavigate } from "react-router-dom";

const TicketPage = () => {
  const api = useApi();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const createNewTicket = async (data) => {
    try {
      const response = await api.post("/tickets/", data);
      dispatch(AddTicket(response.data));
    } catch (error) {
      console.error(error);
    }
  };

  const updateTicket = async (data) => {
    try {
      const response = await api.put(`/tickets/${data.id}`, data);
      dispatch(UpdateTicket(response.data));
    } catch (error) {
      console.error(error);
    }
  };

  const deleteTicket = async (id) => {
    try {
      console.log(id);
      const response = await api.delete(`/tickets/${id}`);
      dispatch(DeleteTicket(id));
    } catch (error) {
      console.error(error);
    }
  };

  const logout = async () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("refreshToken");
    navigate("/login");
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}>
        <h1>Gestion de Tickets</h1>
        <button onClick={logout}>logout</button>
      </div>
      <TicketForm onSubmit={createNewTicket} />
      <br />
      <hr />
      <br />
      <TicketList onUpdate={updateTicket} onDelete={deleteTicket} />
    </div>
  );
};

export default TicketPage;
