import { combineReducers } from "@reduxjs/toolkit";

import ticketsReducer from "../slices/ticketsSlice";

export default combineReducers({
  tickets: ticketsReducer,
});
