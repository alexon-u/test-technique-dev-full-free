import { createSlice } from "@reduxjs/toolkit";
import Ticket from "../../types/ticket"
import { RootState } from "..";
// *  Writing the Slices

//+ createSlice returns a "slice" object that contains the generated reducer function as a field named reducer,
//+ and the generated action creators inside an object called actions.

// Define a type for the slice state
interface TicketState {
  data: Ticket[],
  filteredData: Ticket[]
}

// Define the initial state using that type
const initialState: TicketState = {
  data: [],
  filteredData: []
}

export const basketsSlice = createSlice({
  name: "tickets",
  initialState,
  reducers: {
    fillTickets(state, action) {
      state.data = action.payload;
    },

    AddTicket(state, action) {
      state.data.push(action.payload);
    },

    UpdateTicket(state, action) {
      const ticketIdx = state.data.findIndex(
        (ticket) => ticket.id === action.payload.id
      );

      if (ticketIdx !== -1) {
        state.data[ticketIdx] = action.payload;
      }
    },

    DeleteTicket(state, action) {
      const ticketIdx = state.data.findIndex(
        (ticket) => ticket.id === action.payload
      );

      if (ticketIdx !== -1) {
        state.data.splice(ticketIdx, 1);
      }
    },

    SearchTicketByZone(state, action) {
      state.filteredData = state.data.filter(x => x.zone.includes(action.payload));
    },
  },
});

//+ generated action creator functions :return an object with payload and type
export const {
  fillTickets,
  AddTicket,
  UpdateTicket,
  DeleteTicket
} = basketsSlice.actions;

// useSelector(state => state.baskets) :returns the state for baskets
export const selectTickets = (state: RootState) => state.tickets.data;
export const selectFilteredData = (state: RootState) => state.tickets.filteredData;

// + the generated reducer function
export default basketsSlice.reducer;
