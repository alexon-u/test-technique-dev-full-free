import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { fillTickets, selectTickets } from "../store/slices/ticketsSlice";
import useApi from "../hooks/useApi";
import "./TicketList.css";
import Modal from "react-modal";

const TicketList = ({ onUpdate, onDelete }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentTicket, setCurrentTicket] = useState(null);
  const dispatch = useAppDispatch();
  const tickets = useAppSelector(selectTickets);
  const [filteredTickets, setFilteredTickets] = useState(tickets);
  const api = useApi();
  const [filterZone, setFilterZone] = useState("");
  const [sortZone, setSortZone] = useState("");

  const getAllTickets = async () => {
    try {
      const response = await api.get("/tickets/");
      dispatch(fillTickets(response.data));
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getAllTickets();
  }, []);

  const handleUpdateClick = (ticket) => {
    setCurrentTicket(ticket);
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
    setCurrentTicket(null);
  };

  const handleModalSubmit = (event) => {
    event.preventDefault();
    onUpdate(currentTicket);
    handleModalClose();
  };

  useEffect(() => {
    let newTickets = [...tickets];

    // Filter tickets by zone
    if (filterZone) {
      newTickets = newTickets.filter((ticket) =>
        ticket.zone.toLowerCase().includes(filterZone.toLowerCase())
      );
    }

    // Sort tickets by ID
    if (sortZone) {
      newTickets.sort((a, b) => {
        if (sortZone === "asc") {
          return a.id - b.id;
        } else {
          return b.id - a.id;
        }
      });
    }

    setFilteredTickets(newTickets);
  }, [tickets, filter, sortOrder]);

  useEffect(() => {
    Modal.setAppElement("#root");
  }, []);

  return (
    <>
      <div>
        <label>
          Filter by zone:
          <input
            value={filterZone}
            onChange={(e) => setFilterZone(e.target.value)}
          />
        </label>
        <label style={{ paddingLeft: "10px" }}>
          Sort by zone:
          <select
            value={sortZone}
            onChange={(e) => setSortZone(e.target.value)}>
            <option value='' selected>
              Select...
            </option>
            <option value='asc'>Ascending</option>
            <option value='desc'>Descending</option>
          </select>
        </label>
      </div>

      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Priority</th>
            <th>Zone</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredTickets.map((ticket) => (
            <tr key={ticket.id}>
              <td>{ticket.id}</td>
              <td>{ticket.priority}</td>
              <td>{ticket.zone}</td>
              <td>
                <button onClick={() => handleUpdateClick(ticket)}>
                  Update
                </button>
                <button onClick={() => onDelete(ticket.id)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      {currentTicket && (
        <Modal isOpen={isModalOpen} onRequestClose={handleModalClose}>
          <h2>Update Ticket</h2>
          <form onSubmit={handleModalSubmit}>
            <label>
              Id:
              <input type='number' value={currentTicket.id} disabled={true} />
            </label>

            <label>
              Priority:
              <input
                type='number'
                value={currentTicket.priority}
                onChange={(e) =>
                  setCurrentTicket({
                    ...currentTicket,
                    priority: e.target.value,
                  })
                }
              />
            </label>
            <label>
              Zone:
              <input
                type='text'
                value={currentTicket.zone}
                onChange={(e) =>
                  setCurrentTicket({ ...currentTicket, zone: e.target.value })
                }
              />
            </label>
            <button type='submit'>Submit</button>
          </form>
        </Modal>
      )}
    </>
  );
};

export default TicketList;
