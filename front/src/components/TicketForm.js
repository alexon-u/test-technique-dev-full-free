import { jsx as _jsx, jsxs as _jsxs } from "react/jsx-runtime";
import { useForm } from "react-hook-form";
const TicketForm = ({ onSubmit }) => {
    const { register, handleSubmit, formState: { errors }, } = useForm();
    const PRIORITY_CHOICES = [
        { value: 1, label: "1 - High priority" },
        { value: 2, label: "2 - Medium priority" },
        { value: 3, label: "3 - Low priority" },
    ];
    const ZONE_CHOICES = [...Array(26)].map((_, i) => String.fromCharCode(i + 65));
    return (_jsxs("form", { onSubmit: handleSubmit(onSubmit), children: [_jsxs("label", { children: ["Priority:", _jsx("select", { ...register("priority", { required: "Priority is required" }), children: PRIORITY_CHOICES.map((choice) => (_jsx("option", { value: choice.value, children: choice.label }, choice.value))) }), errors.priority && _jsx("p", { children: errors.priority.message })] }), _jsxs("label", { style: { paddingLeft: "10px" }, children: ["Zone:", _jsx("select", { ...register("zone", { required: "Zone is required" }), children: ZONE_CHOICES.map((choice) => (_jsx("option", { value: choice, children: choice }, choice))) }), errors.zone && _jsx("p", { children: errors.zone.message })] }), _jsx("button", { style: { marginLeft: "15px" }, type: 'submit', children: "Submit" })] }));
};
export default TicketForm;
