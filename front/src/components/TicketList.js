import { jsx as _jsx, jsxs as _jsxs, Fragment as _Fragment } from "react/jsx-runtime";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../store/hooks";
import { fillTickets, selectTickets } from "../store/slices/ticketsSlice";
import useApi from "../hooks/useApi";
import "./TicketList.css";
import Modal from "react-modal";
const TicketList = ({ onUpdate, onDelete }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [currentTicket, setCurrentTicket] = useState(null);
    const dispatch = useAppDispatch();
    const tickets = useAppSelector(selectTickets);
    const api = useApi();
    const [filterZone, setFilterZone] = useState("");
    const [sortZone, setSortZone] = useState("asc");
    const getAllTickets = async () => {
        try {
            const response = await api.get("/tickets/");
            dispatch(fillTickets(response.data));
        }
        catch (error) {
            console.error(error);
        }
    };
    useEffect(() => {
        getAllTickets();
    }, []);
    const handleUpdateClick = (ticket) => {
        setCurrentTicket(ticket);
        setIsModalOpen(true);
    };
    const handleModalClose = () => {
        setIsModalOpen(false);
        setCurrentTicket(null);
    };
    const handleModalSubmit = (event) => {
        event.preventDefault();
        onUpdate(currentTicket);
        handleModalClose();
    };
    const filteredTickets = tickets.filter((ticket) => ticket.zone.includes(filterZone));
    const sortedTickets = filteredTickets.sort((a, b) => {
        if (sortZone === "asc") {
            return a.zone.localeCompare(b.zone);
        }
        else {
            return b.zone.localeCompare(a.zone);
        }
    });
    useEffect(() => {
        Modal.setAppElement("#root");
    }, []);
    return (_jsxs(_Fragment, { children: [_jsxs("div", { children: [_jsxs("label", { children: ["Filter by zone:", _jsx("input", { value: filterZone, onChange: (e) => setFilterZone(e.target.value) })] }), _jsxs("label", { style: { paddingLeft: "10px" }, children: ["Sort by zone:", _jsxs("select", { value: sortZone, onChange: (e) => setSortZone(e.target.value), children: [_jsx("option", { value: 'asc', children: "Ascending" }), _jsx("option", { value: 'desc', children: "Descending" })] })] })] }), _jsxs("table", { children: [_jsx("thead", { children: _jsxs("tr", { children: [_jsx("th", { children: "ID" }), _jsx("th", { children: "Priority" }), _jsx("th", { children: "Zone" }), _jsx("th", { children: "Actions" })] }) }), _jsx("tbody", { children: sortedTickets.map((ticket) => (_jsxs("tr", { children: [_jsx("td", { children: ticket.id }), _jsx("td", { children: ticket.priority }), _jsx("td", { children: ticket.zone }), _jsxs("td", { children: [_jsx("button", { onClick: () => handleUpdateClick(ticket), children: "Update" }), _jsx("button", { onClick: () => onDelete(ticket.id), children: "Delete" })] })] }, ticket.id))) })] }), currentTicket && (_jsxs(Modal, { isOpen: isModalOpen, onRequestClose: handleModalClose, children: [_jsx("h2", { children: "Update Ticket" }), _jsxs("form", { onSubmit: handleModalSubmit, children: [_jsxs("label", { children: ["Id:", _jsx("input", { type: 'number', value: currentTicket.id, disabled: true })] }), _jsxs("label", { children: ["Priority:", _jsx("input", { type: 'number', value: currentTicket.priority, onChange: (e) => setCurrentTicket({
                                            ...currentTicket,
                                            priority: e.target.value,
                                        }) })] }), _jsxs("label", { children: ["Zone:", _jsx("input", { type: 'text', value: currentTicket.zone, onChange: (e) => setCurrentTicket({ ...currentTicket, zone: e.target.value }) })] }), _jsx("button", { type: 'submit', children: "Submit" })] })] }))] }));
};
export default TicketList;
