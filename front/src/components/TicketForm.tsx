import { useForm } from "react-hook-form";

const TicketForm = ({ onSubmit }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const PRIORITY_CHOICES = [
    { value: 1, label: "1 - High priority" },
    { value: 2, label: "2 - Medium priority" },
    { value: 3, label: "3 - Low priority" },
  ];

  const ZONE_CHOICES = [...Array(26)].map((_, i) =>
    String.fromCharCode(i + 65)
  );

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <label>
        Priority:
        <select {...register("priority", { required: "Priority is required" })}>
          {PRIORITY_CHOICES.map((choice) => (
            <option key={choice.value} value={choice.value}>
              {choice.label}
            </option>
          ))}
        </select>
        {errors.priority && <p>{errors.priority.message}</p>}
      </label>

      <label style={{ paddingLeft: "10px" }}>
        Zone:
        <select {...register("zone", { required: "Zone is required" })}>
          {ZONE_CHOICES.map((choice) => (
            <option key={choice} value={choice}>
              {choice}
            </option>
          ))}
        </select>
        {errors.zone && <p>{errors.zone.message}</p>}
      </label>

      <button style={{ marginLeft: "15px" }} type='submit'>
        Submit
      </button>
    </form>
  );
};

export default TicketForm;
