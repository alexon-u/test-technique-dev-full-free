import { jsx as _jsx, jsxs as _jsxs, Fragment as _Fragment } from "react/jsx-runtime";
import "./App.css";
import { BrowserRouter as Router, Route, Routes, Navigate, } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import TicketPage from "./pages/TicketPage";
import LoginPage from "./pages/LoginPage";
function App() {
    return (_jsx(_Fragment, { children: _jsx(Router, { children: _jsxs(Routes, { children: [_jsx(Route, { path: '/login', element: _jsx(LoginPage, {}) }), _jsx(Route, { path: '/tickets', element: _jsx(PrivateRoute, { children: _jsx(TicketPage, {}) }) }), _jsx(Route, { path: '*', element: _jsx(Navigate, { to: '/login' }) })] }) }) }));
}
export default App;
