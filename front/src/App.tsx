import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import TicketPage from "./pages/TicketPage";
import LoginPage from "./pages/LoginPage";

function App() {
  console.log("API_URL", import.meta.env.VITE_API_URL);

  return (
    <>
      <Router>
        <Routes>
          <Route path='/login' element={<LoginPage />} />
          <Route
            path='/tickets'
            element={
              <PrivateRoute>
                <TicketPage />
              </PrivateRoute>
            }
          />
          <Route path='*' element={<Navigate to='/login' />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
