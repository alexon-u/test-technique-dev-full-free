#### Pour allumer l'app en local
```
$ cd front
$ npm i
$ npm run dev
```

#### Fonctionnalités implementés dans cette partie
- login page
- useApi hooks : Gestion des tokens (acces et refresh token) et les requêtes http
- CRUD sur les tickets